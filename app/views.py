from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, 'app/index.html')


lista = [
    {'id': 0, 'nombre': 'Pulp Fiction', 'year': 1998},
    {'id': 1, 'nombre': 'The Hateful Eight', 'year': 2015},
    {'id': 2, 'nombre': 'Django Unchained', 'year': 2010},
    {'id': 3, 'nombre': 'Kill Bill Vol. 1', 'year': 2004},
]

def peliculas(request):
    contexto = {
        'lista_peliculas': lista,
        'year': 2020,
        'tienda': 'Bluckbuster'
    }
    return render(request, 'app/peliculas.html',contexto)

def pelicula(request, id):
    contexto = {}
    if id < len(id):
        contexto['pelicula'] = lista[id]
    return render(request, 'app/pelicula.html', contexto)